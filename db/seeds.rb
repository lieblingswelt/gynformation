# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Tag.create(
  key: 'trans_friendly',
  name: { en: 'Trans friendly', de: 'Trans freundlich' }
)
Tag.create(
  key: 'queer_friendly',
  name: { en: 'Queer friendly', de: 'Queer freundlich' }
)

TreatmentMethod.create(
  name: { en: 'Abortion', de: 'Schwangerschaftsabbruch' },
  key: 'abortion'
)
TreatmentMethod.create(
  name: { en: 'Sterilisation', de: 'Sterilisation' },
  key: 'sterilisation'
)

if Rails.env.development?
  20.times do
    profile = Profile.create!(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      website: Faker::Internet.domain_name,
      category: Random.new.rand(0..2),
      published: [true, false].sample,
      comment: Faker::Quote.yoda,
      language: %w[English German Spanish].sample,
      health_insurance: %i[statutory uninsured],
      phone_number: Faker::PhoneNumber.phone_number,
      accessibility: Faker::Lorem.sentence
    )
    profile.create_address!(
      street: Faker::Address.street_address,
      zip: Faker::Address.zip,
      city: Faker::Address.city,
      state: %w[DE-HH DE-MV DE-HB DE-SL].sample
    )
    Tagging.create!(tag_id: Tag.all.sample.id, profile_id: profile.id)
    ProfileTreatment.create(treatment_method_id: TreatmentMethod.all.sample.id, profile_id: profile.id)
    questionnaire = Questionnaire.new
    questionnaire.answers.build(
      key: 'last_name', content: Faker::Name.first_name,
    )
    questionnaire.answers.build(
      key: 'first_name', content: Faker::Name.last_name,
    )
    questionnaire.save!
  end
  User.create(email: 'admin@example.org', password: 'password')
end
