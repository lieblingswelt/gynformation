class ChangeTagNameToJsonb < ActiveRecord::Migration[5.2]
  def change
    change_column :tags, :name, :jsonb, using: 'name::jsonb',  default: '{}'
  end
end
