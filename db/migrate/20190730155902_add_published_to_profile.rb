# frozen_string_literal: true

class AddPublishedToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :published, :boolean
  end
end
