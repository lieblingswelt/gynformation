class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :name
      t.integer :category
      t.string :comment

      t.timestamps
    end
  end
end
