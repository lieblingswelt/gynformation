class AddStateIndexToAddress < ActiveRecord::Migration[5.2]
  def change
    add_index :addresses, :state
  end
end
