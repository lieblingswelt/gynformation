class AddAttributesToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :phone_number, :string
    add_column :profiles, :gender, :string
    add_column :profiles, :accessibility, :string
    add_column :profiles, :financing_of_treatments, :string
  end
end
