class CreateProfileTreatments < ActiveRecord::Migration[5.2]
  def change
    create_table :profile_treatments do |t|
      t.belongs_to :treatment_method, foreign_key: true
      t.belongs_to :profile, foreign_key: true

      t.timestamps
    end
  end
end
