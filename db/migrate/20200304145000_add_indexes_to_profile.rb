class AddIndexesToProfile < ActiveRecord::Migration[5.2]
  def change
    add_index :profiles, :last_name
    add_index :profiles, :published
  end
end
