class AddDateToTreatmentMethod < ActiveRecord::Migration[5.2]
  def change
    add_column :treatment_methods, :created_at, :datetime, null: false
    add_column :treatment_methods, :updated_at, :datetime, null: false
  end
end
