class RemoveCommentFromProfile < ActiveRecord::Migration[5.2]
  def change
    remove_column :profiles, :comment, :string
  end
end
