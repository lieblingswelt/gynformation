class AddKeyToTreatmentMethod < ActiveRecord::Migration[5.2]
  def change
    add_column :treatment_methods, :key, :string
  end
end
