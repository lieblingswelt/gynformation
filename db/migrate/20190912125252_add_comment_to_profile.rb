class AddCommentToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :comment, :string
  end
end
