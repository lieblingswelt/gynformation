# frozen_string_literal: true

class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street
      t.string :city
      t.integer :zip
      t.string :state
      t.integer :profile_id

      t.timestamps
    end
    add_index :addresses, :profile_id
  end
end
