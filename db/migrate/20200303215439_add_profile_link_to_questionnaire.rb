class AddProfileLinkToQuestionnaire < ActiveRecord::Migration[5.2]
  def change
    add_column :questionnaires, :profile_link, :string
  end
end
