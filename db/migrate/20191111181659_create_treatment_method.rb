class CreateTreatmentMethod < ActiveRecord::Migration[5.2]
  def change
    create_table :treatment_methods do |t|
        t.jsonb :name
    end
  end
end
