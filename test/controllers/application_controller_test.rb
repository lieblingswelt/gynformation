# frozen_string_literal: true

require 'test_helper'

class ApplicationControllerTest < ActionDispatch::IntegrationTest
  test 'basic authentication' do
    skip 'Not working in CI'
    auth_headers = { 'Authorization' => "Basic #{Base64.encode64("#{Rails
      .application.credentials.basic_auth[:username]}:#{Rails
      .application.credentials.basic_auth[:password]}")}" }

    env = ENV.to_hash.merge('BASIC_AUTH' => 'enabled')

    Object.stub_const(:ENV, env) do
      get root_path
      assert_response 401
      get root_path, headers: auth_headers
      assert_response :success
    end
  end
end
