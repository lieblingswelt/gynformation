# frozen_string_literal: true

require 'test_helper'

class Admin::PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = create(:post)
    sign_in_as_admin
  end

  test 'should get index' do
    get admin_posts_path
    assert_response :success
  end

  test 'should get new' do
    get new_admin_post_path
    assert_response :success
  end

  test 'should create post' do
    assert_difference('Post.count') do
      post admin_posts_url, params: {
        post: {
          author: @post.author,
          body: @post.body,
          teaser: @post.teaser,
          title: @post.title
        }
      }
    end

    assert_redirected_to admin_post_path(Post.last)
  end

  test 'should show post' do
    get admin_post_path(@post)
    assert_response :success
  end

  test 'should get edit' do
    get edit_admin_post_path(@post)
    assert_response :success
  end

  test 'should update post' do
    patch admin_post_path(@post), params: {
      post: {
        author: @post.author,
        body: @post.body,
        teaser: @post.teaser,
        title: @post.title
      }
    }
    assert_redirected_to admin_post_path(@post)
  end

  test 'should destroy post' do
    assert_difference('Post.count', -1) do
      delete admin_post_path(@post)
    end

    assert_redirected_to admin_posts_path
  end

  def sign_in_as_admin
    user = create(:user)
    sign_in_as(user)
  end
end
