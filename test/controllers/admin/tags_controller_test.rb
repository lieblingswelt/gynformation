# frozen_string_literal: true

require 'test_helper'

class Admin::TagsControllerTest < ActionDispatch::IntegrationTest
  def setup
    user = create(:user)
    sign_in_as(user)
  end

  test 'should get index' do
    get admin_tags_path

    assert_response 200
  end

  test 'should get new when user is signed in' do
    user = create(:user)
    sign_in_as(user)

    get new_admin_tag_path

    assert_response 200
  end

  test 'should create tag' do
    user = create(:user)
    sign_in_as(user)

    assert_difference('Tag.count', 1) do
      post admin_tags_url, params: { 'tag' => {
        'en' => 'Sex worker friendly',
        'de' => 'Sexworkerinnen freundlich'
      } }
    end

    assert_redirected_to admin_tags_path
  end

  test 'should save key when creating tag' do
    user = create(:user)
    sign_in_as(user)

    post admin_tags_url, params: { 'tag' => {
      'en' => 'Sex worker friendly',
      'de' => 'Sexworkerinnen freundlich'
    } }

    created_tag = Tag.last
    created_tag.reload
    assert_equal created_tag.key, 'sex_worker_friendly'
  end

  test 'should update tag' do
    user = create(:user)
    sign_in_as(user)
    tag = create(:tag)

    patch admin_tag_url(tag.id), params: { 'tag' => {
      'en' => 'New en tag name',
      'de' => 'New de tag name'
    } }

    updated_tag = Tag.last
    updated_tag.reload

    assert_equal updated_tag.en, 'New en tag name'
  end
end
