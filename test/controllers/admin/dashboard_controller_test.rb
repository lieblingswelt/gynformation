# frozen_string_literal: true

require 'test_helper'

class Admin::DashboardControllerTest < ActionDispatch::IntegrationTest
  test 'should get index when user is signed in' do
    user = create(:user)
    sign_in_as(user)

    get admin_dashboard_index_path

    assert_response 200
    assert_includes @response.body, 'Administration'
  end

  test 'index filters by published state' do
    user = create(:user)
    sign_in_as(user)
    published_profile = create(:published_profile)
    unpublished_profile = create(:unpublished_profile)

    get admin_dashboard_index_path, params: { published: 'true' }

    assert_includes @response.body, published_profile.last_name
    assert_not_includes @response.body, unpublished_profile.last_name
  end

  test 'should return a routing error when no user is signed in' do
    assert_raise(ActionController::RoutingError) do
      get admin_dashboard_index_path
    end
  end
end
