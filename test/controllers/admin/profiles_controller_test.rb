# frozen_string_literal: true

require 'test_helper'

class Admin::ProfilesControllerTest < ActionDispatch::IntegrationTest
  test 'should get new when user is signed in' do
    user = create(:user)
    sign_in_as(user)

    get new_admin_profile_path

    assert_response 200
  end

  test 'should return a routing error on get /new for a visitor' do
    assert_raise(ActionController::RoutingError) do
      get new_admin_profile_path
    end
  end

  test 'should create profile' do
    user = create(:user)
    sign_in_as(user)

    assert_difference('Profile.count', 1) do
      post admin_profiles_url, params: { profile: {
        first_name: 'Lora',
        last_name: 'Ipsum',
        website: 'www.example.org',
        category: :midwife
      } }
    end

    assert_redirected_to admin_profile_path(Profile.last)
  end

  test 'should return a routing error on /create for a visitor' do
    assert_raise(ActionController::RoutingError) do
      post admin_profiles_url, params: { profile: {
        first_name: 'Lora',
        last_name: 'Ipsum',
        website: 'www.example.org',
        category: :midwife
      } }
    end
  end

  test 'should not create profile without last name' do
    user = create(:user)
    sign_in_as(user)

    assert_difference('Profile.count', 0) do
      post admin_profiles_url, params: { profile: {
        website: 'www.example.org',
        category: :midwife
      } }
    end

    assert_includes @response.body, 'Please provide a last name'
  end

  test 'successful edit' do
    profile = create(:profile, last_name: 'Before')
    new_last_name = 'A new name'
    user = create(:user)
    sign_in_as(user)

    patch admin_profile_path(profile), params: {
      profile: { last_name: new_last_name }
    }

    assert_not flash.empty?
    assert_redirected_to admin_profile_path(profile)
    profile.reload
    assert_equal new_last_name, profile.last_name
  end

  test 'delete profile' do
    user = create(:user)
    sign_in_as(user)
    profile = create(:profile)

    assert_difference('Profile.count', -1) do
      delete admin_profile_path(profile)
    end

    assert_redirected_to admin_dashboard_index_path
  end

  test 'should get profile' do
    user = create(:user)
    sign_in_as(user)
    @profile = create(:profile)
    get admin_profile_path(@profile.id)

    assert_response 200
    assert_includes @response.body, @profile.website
  end
end
