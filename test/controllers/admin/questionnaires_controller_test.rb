# frozen_string_literal: true

require 'test_helper'

class Admin::QuestionnairesControllerTest < ActionDispatch::IntegrationTest
  def setup
    user = create(:user)
    sign_in_as(user)
  end

  test 'should get index' do
    get admin_questionnaires_path

    assert_response 200
  end

  test 'should get edit' do
    questionnaire = create(:questionnaire)

    get edit_admin_questionnaire_path(questionnaire.id)

    assert_response 200
  end

  test 'successful edit' do
    questionnaire = create(:questionnaire,
                           comment: 'Some comment',
                           status: 'new')
    new_status = 'profile_published'
    new_comment = 'Something new'
    new_profile_link = 'www.new-link.org'
    user = create(:user)
    sign_in_as(user)

    patch admin_questionnaire_path(questionnaire.id), params: {
      questionnaire: {
        comment: new_comment,
        status: new_status,
        profile_link: new_profile_link
      }
    }

    assert_not flash.empty?
    assert_redirected_to edit_admin_questionnaire_path(questionnaire)
    questionnaire.reload
    assert_equal new_status, questionnaire.status
    assert_equal new_comment, questionnaire.comment
    assert_equal new_profile_link, questionnaire.profile_link
  end

  test 'delete questionnaire' do
    user = create(:user)
    sign_in_as(user)
    answer = create(:answer)
    questionnaire = create(:questionnaire, answers: [answer])

    assert_difference('Questionnaire.count', -1) do
      assert_difference('Answer.count', -1) do
        delete admin_questionnaire_path(questionnaire)
      end
    end

    assert_redirected_to admin_questionnaires_path
  end
end
