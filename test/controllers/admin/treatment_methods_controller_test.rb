# frozen_string_literal: true

require 'test_helper'

class Admin::TreatmentMethodsControllerTest < ActionDispatch::IntegrationTest
  def setup
    user = create(:user)
    sign_in_as(user)
  end

  test 'should get index' do
    get admin_treatment_methods_path

    assert_response 200
  end

  test 'should get new when user is signed in' do
    user = create(:user)
    sign_in_as(user)

    get new_admin_treatment_method_path

    assert_response 200
  end

  test 'should create treatment_method' do
    user = create(:user)
    sign_in_as(user)

    assert_difference('TreatmentMethod.count', 1) do
      post admin_treatment_methods_url, params: { 'treatment_method' => {
        'en' => 'English Treatment method',
        'de' => 'German Treatment method'
      } }
    end

    assert_redirected_to admin_treatment_methods_path
  end

  test 'should save key when creating treatment_method' do
    user = create(:user)
    sign_in_as(user)

    post admin_treatment_methods_url, params: { 'treatment_method' => {
      'en' => 'English Treatment method',
      'de' => 'German Treatment method'
    } }

    created_treatment_method = TreatmentMethod.last
    created_treatment_method.reload
    assert_equal created_treatment_method.key, 'english_treatment_method'
  end

  test 'should update treatment_method' do
    user = create(:user)
    sign_in_as(user)
    treatment_method = create(:treatment_method)

    patch admin_treatment_method_url(treatment_method.id),
          params: { 'treatment_method' => {
            'en' => 'New en treatment_method name',
            'de' => 'New de treatment_method name'
          } }

    updated_treatment_method = TreatmentMethod.last
    updated_treatment_method.reload

    assert_equal updated_treatment_method.en, 'New en treatment_method name'
  end
end
