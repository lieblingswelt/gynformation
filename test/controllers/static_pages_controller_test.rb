# frozen_string_literal: true

require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test 'should get home' do
    get static_pages_home_path
    assert_response :success
  end

  test 'should get about' do
    get about_path
    assert_response :success
  end

  test 'should get faq' do
    get faq_path
    assert_response :success
  end

  test 'should get imprint' do
    get imprint_path
    assert_response :success
  end

  test 'should get data-protection' do
    get data_protection_path
    assert_response :success
  end

  test 'should get contact' do
    get contact_path
    assert_response :success
  end

  test 'should get glossary' do
    get glossary_path
    assert_response :success
  end
end
