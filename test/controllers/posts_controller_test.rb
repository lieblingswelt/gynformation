# frozen_string_literal: true

require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = create(:published_post)
  end

  test 'should show post' do
    get post_path(@post)
    assert_response :success
  end

  test 'should get index' do
    get posts_path
    assert_response :success
  end
end
