# frozen_string_literal: true

require 'test_helper'

class ProfilesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @profile = create(:published_profile)
    @address = create(:address, state: 'DE-BW', profile_id: @profile.id)
    @tag = create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    @treatment_method = create(:treatment_method, name: {
                                 en: 'Abortion', de: 'Schwangerschaftsabbruch'
                               })
    create(:profile_treatment, treatment_method_id: @treatment_method.id,
                               profile_id: @profile.id)
    create(:tagging, tag_id: @tag.id, profile_id: @profile.id)
  end

  test 'should get index' do
    get profiles_path
    assert_response 200
    assert_includes @response.body, @profile.last_name
  end

  test 'should not index unpublished profiles' do
    unpublished_profile = create(:unpublished_profile)
    get profiles_path
    assert_response 200
    assert_not_includes @response.body, unpublished_profile.last_name
  end

  test 'index filters by tag' do
    second_profile = create(
      :profile,
      last_name: 'Second Profile'
    )
    second_tag = create(:tag,
                        name: { en: 'Queer friendly', de: 'Queer freundlich' })
    create(:tagging, tag_id: second_tag.id, profile_id: second_profile.id)

    get profiles_path, params: { tag: 'trans_friendly' }

    assert_includes @response.body, @profile.last_name
    assert_not_includes @response.body, second_profile.last_name
  end

  test 'index filters by tag, state and treatment_method' do
    second_profile = create(:profile, last_name: 'Second Profile')
    second_tag = create(:tag,
                        name: { en: 'Queer friendly', de: 'Queer freundlich' })
    create(:tagging, tag_id: second_tag.id, profile_id: second_profile.id)
    create(:address, state: 'DE-HH', profile_id: second_profile.id)
    @treatment_method = create(:treatment_method,
                               name: { en: 'Sterilization',
                                       de: 'Sterilisation' },
                               key: 'sterilization')
    create(:profile_treatment, treatment_method_id: @treatment_method.id,
                               profile_id: second_profile.id)

    get profiles_path, params: {
      tag: 'trans_friendly', state: 'DE-BW', treatment_method: 'abortion'
    }

    assert_includes @response.body, @profile.last_name
    assert_not_includes @response.body, second_profile.last_name
  end

  test 'should get profile' do
    get profile_path(@profile.id)

    assert_response 200
    assert_includes @response.body, @profile.website
  end
end
