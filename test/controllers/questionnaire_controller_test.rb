# frozen_string_literal: true

require 'test_helper'

class QuestionnaireControllerTest < ActionDispatch::IntegrationTest
  test 'should get new' do
    get new_questionnaire_path

    assert_response 200
  end

  test 'should get info' do
    get questionnaire_info_path
    assert_response 200
  end

  test 'should create questionnaire and answers' do
    assert_difference('Questionnaire.count', 1) do
      assert_difference('Answer.count', 29) do
        post questionnaire_index_path, params: {
          questionnaire_form: {
            first_name: 'Lora',
            body: 'Ipsum'
          }
        }
      end
    end

    assert_redirected_to questionnaire_info_path
  end
end
