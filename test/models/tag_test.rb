# frozen_string_literal: true

require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test 'name returns name for a given locale' do
    tag = Tag.create(name: { en: 'Trans friendly', de: 'Trans freundlich' })
    german_tag_name = tag.name['de']

    assert_equal german_tag_name, 'Trans freundlich'
  end
end
