# frozen_string_literal: true

require 'test_helper'

class AnswerTest < ActiveSupport::TestCase
  test 'sorts ascending by created at' do
    first_answer = create(:answer)
    second_answer = create(:answer)

    assert_equal first_answer, Answer.first
    assert_equal second_answer, Answer.last
  end

  test '#answer returns the content of the answer with the given key' do
    answer = create(:answer, key: 'a-key', content: 'This is the answer')
    questionnaire = create(:questionnaire, answers: [answer])

    assert_equal questionnaire.answer('a-key'), 'This is the answer'
  end

  test '#answer returns an empty hash {} for a non existing key' do
    questionnaire = create(:questionnaire)

    assert_equal questionnaire.answer('non-existing-key'), ''
  end
end
