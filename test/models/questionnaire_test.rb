# frozen_string_literal: true

require 'test_helper'

class QuestionnaireTest < ActiveSupport::TestCase
  test 'sorts by recently created' do
    first_questionnaire = create(:questionnaire)
    second_questionnaire = create(:questionnaire)

    assert_equal second_questionnaire, Questionnaire.first
    assert_equal first_questionnaire, Questionnaire.last
  end

  test 'returns the full name of the treating persons' do
    answer1 = create(:answer, key: 'first_name', content: 'Hanni')
    answer2 = create(:answer, key: 'last_name', content: 'Example')
    questionnaire = create(:questionnaire, answers: [answer1, answer2])

    assert_equal 'Hanni Example', questionnaire.treating_persons_name
  end

  test 'treating_persons_name returns nil when names are not present' do
    questionnaire = create(:questionnaire)

    assert_nil questionnaire.treating_persons_name
  end

  test 'invalid status' do
    exception = assert_raises ArgumentError do
      create(:questionnaire, status: :invalid)
    end

    assert_equal("'invalid' is not a valid status", exception.message)
  end

  test 'sets "new" as default value for status' do
    questionnaire = Questionnaire.new

    assert_equal 'new', questionnaire.status
  end

  test '#filter_by_status' do
    questionnaire1 = create(:questionnaire, status: :new)
    questionnaire2 = create(:questionnaire, status: :profile_published)

    assert_includes Questionnaire.filter_by_status('new'), questionnaire1
    refute_includes Questionnaire.filter_by_status('new'), questionnaire2
  end
end
