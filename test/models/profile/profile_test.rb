# frozen_string_literal: true

require 'test_helper'

class ProfileTest < ActiveSupport::TestCase
  test 'valid profile' do
    profile = Profile.new(
      first_name: 'Carla',
      last_name: 'Smith',
      category: :gynecologist,
      website: 'www.example.org'
    )
    assert profile.valid?
  end

  test 'invalid without name' do
    profile = Profile.new(category: :gynecologist, website: 'www.example.org')
    assert_not profile.valid?
  end

  test 'invalid category' do
    exception = assert_raises ArgumentError do
      Profile.new(first_name: 'Carla', last_name: 'Smith', category: :invalid)
    end
    assert_equal("'invalid' is not a valid category", exception.message)
  end

  test 'should set published to false by default' do
    profile = create(:profile)

    assert_equal profile.published, false
  end

  test '#filter_by_tag' do
    profile = create(:profile)
    untagged_profile = create(:profile)
    tag = create(:tag, key: 'trans_friendly')
    create(:tagging, tag_id: tag.id, profile_id: profile.id)

    assert_includes Profile.filter_by_tag('trans_friendly'), profile
    refute_includes Profile.filter_by_tag('trans_friendly'), untagged_profile
  end

  test '#filter_by_treatment_method' do
    profile = create(:profile)
    untagged_profile = create(:profile)
    treatment_method = create(:treatment_method, key: 'abortion')
    create(:profile_treatment, treatment_method_id: treatment_method.id,
                               profile_id: profile.id)

    assert_includes Profile.filter_by_treatment_method('abortion'), profile
    refute_includes Profile.filter_by_treatment_method('abortion'),
                    untagged_profile
  end

  test '#filter_by_state' do
    profile = create(:profile)
    create(:address, profile_id: profile.id, state: 'DE-HH')
    profile_with_different_state = create(:profile)

    assert_includes Profile.filter_by_state('DE-HH'), profile
    refute_includes Profile.filter_by_state('DE-HH'),
                    profile_with_different_state
  end

  test '#filter_by_published' do
    published_profile = create(:published_profile)
    unpublished_profile = create(:unpublished_profile)

    assert_includes Profile.filter_by_published('true'), published_profile
    refute_includes Profile.filter_by_published('true'), unpublished_profile
  end

  test '#filter_by_last_name' do
    profile1 = create(:profile, last_name: 'Meier')
    profile2 = create(:profile, last_name: 'Schmidt')

    assert_includes Profile.filter_by_last_name('Meier'), profile1
    refute_includes Profile.filter_by_published('Meier'), profile2
  end
end
