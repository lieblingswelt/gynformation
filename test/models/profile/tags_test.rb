# frozen_string_literal: true

require 'test_helper'

class ProfileTagsTest < ActiveSupport::TestCase
  test 'tag list returns all tags of the profile' do
    profile = create(:profile)
    tag1 = create(:tag, key: 'trans_friendly',
                        name: {
                          en: 'Trans friendly',
                          de: 'Trans freundlich'
                        })
    tag2 = create(:tag, key: 'sexworker_friendly',
                        name: {
                          en: 'Sex worker friendly',
                          de: 'Sexworkerinnen freundlich'
                        })
    create(:tagging, tag_id: tag1.id, profile_id: profile.id)
    create(:tagging, tag_id: tag2.id, profile_id: profile.id)

    tag_list = profile.tag_list
    expected_tag_list = '{"de"=>"Trans freundlich", "en"=>"Trans friendly"},'\
    ' {"de"=>"Sexworkerinnen freundlich", "en"=>"Sex worker friendly"}'

    assert_equal expected_tag_list, tag_list
  end
end
