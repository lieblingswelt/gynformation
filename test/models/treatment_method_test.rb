# frozen_string_literal: true

require 'test_helper'

class TreatmentMethodTest < ActiveSupport::TestCase
  test 'name returns name for a given locale' do
    treatment_method = TreatmentMethod.create(name: {
                                                en: 'English treatment method',
                                                de: 'German treatment method'
                                              })
    german_treatment_method_name = treatment_method.name['de']

    assert_equal german_treatment_method_name, 'German treatment method'
  end
end
