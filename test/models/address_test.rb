# frozen_string_literal: true

require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  test 'valid address' do
    profile = create(:profile)
    address = Address.new(
      street: 'Bakerstreet 121',
      zip: '20148',
      city: 'Hamburg',
      state: 'DE-HH',
      profile_id: profile.id
    )

    assert address.valid?
  end

  test 'with invalid state' do
    profile = create(:profile)
    address = Address.new(
      state: 'INVALID',
      profile_id: profile.id
    )

    assert address.invalid?
    assert_equal address.errors[:state], ['INVALID is not a valid state']
  end
end
