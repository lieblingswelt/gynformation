# frozen_string_literal: true

require 'test_helper'

class QuestionnaireFormTest < ActiveSupport::TestCase
  test 'succesful_save' do
    params = {
      'first_name' => 'Lora',
      'last_name' => 'Ipsum',
      'category' => 'general_practitioner',
      'payments_required_for_specific_treatments' => {
        'boolean' => 'true',
        'website' => 'www.example.org'
      }
    }
    questionnaire_form = QuestionnaireForm.new(params)

    assert_difference('Questionnaire.count', 1) do
      assert_difference('Answer.count', 29) do
        questionnaire_form.save
      end
    end
  end
end
