# frozen_string_literal: true

require 'active_support/concern'

module MailHelper
  extend ActiveSupport::Concern

  def assert_mail_sent(recipient, subject, body)
    message = ActionMailer::Base.deliveries.any? do |email|
      email.to == [recipient] &&
        email.subject =~ /#{subject}/i &&
        email.html_part.body =~ /#{body}/ &&
        email.text_part.body =~ /#{body}/
    end

    assert message
  end

  def mail_sent?
    ActionMailer::Base.deliveries.any?
  end
end
