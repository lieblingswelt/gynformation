# frozen_string_literal: true

FactoryBot.define do
  factory :profile do
    first_name { 'Lora' }
    last_name { 'Ipsum' }
    website { 'www.example.org' }
    comment { 'This is a comment' }
    language { 'English' }
    phone_number { '2923483248234234' }
    category { 1 }
    taggings { [] }
    accessibility { 'Some information about accessibility' }
    financing_of_treatments { 'Information about financing of treatments' }
    health_insurance { %i[uninsured statutory] }

    factory :published_profile do
      first_name { 'Petra' }
      last_name { 'Published' }
      website { 'www.example.org' }
      comment { 'This is a comment' }
      language { 'English' }
      published { true }
    end

    factory :unpublished_profile do
      first_name { 'Ute' }
      last_name { 'Unpublished Lastname' }
      website { 'www.example.org' }
      comment { 'Unpublished comment' }
      language { 'Spanish, German' }
      published { false }
    end
  end
end
