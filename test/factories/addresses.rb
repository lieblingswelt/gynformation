# frozen_string_literal: true

FactoryBot.define do
  factory :address do
    street { 'Bakerstreet 121' }
    zip { '20148' }
    city { 'Hamburg' }
    state { 'DE-HH' }
    profile_id { 1 }
  end
end
