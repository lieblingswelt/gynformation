# frozen_string_literal: true

FactoryBot.define do
  factory :treatment_method do
    key { 'abortion' }
    name { { en: 'abortion', de: 'Schwangerschaftsabbruch' } }
  end
end
