# frozen_string_literal: true

FactoryBot.define do
  factory :questionnaire do
    answers { [] }
    status  { 1 }
    comment { 'This is a comment' }
  end
end
