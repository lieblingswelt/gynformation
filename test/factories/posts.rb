# frozen_string_literal: true

FactoryBot.define do
  factory :post, class: 'Post' do
    title { 'This is an awesome blogpost' }
    body do
      'This is the content of the blog post.'
    end
    author { 'Test Author' }
    teaser do
      'This is a teaser to let people now what the blog post is about'
    end
    factory :published_post do
      published { true }
    end
  end
end
