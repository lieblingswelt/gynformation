# frozen_string_literal: true

FactoryBot.define do
  factory :tagging do
    tag { nil }
    profile { nil }
  end
end
