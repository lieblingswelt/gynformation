# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

InvisibleCaptcha.timestamp_enabled = false

class ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods
end

module SignInHelper
  def sign_in_with(email, password)
    visit sign_in_url
    fill_in 'session_email', with: email
    fill_in 'session_password', with: password
    click_button I18n.t('sessions.new.submit')
  end

  def sign_in_as(user)
    post session_url, params: {
      session:
      { email: user.email, password: user.password }
    }
  end

  def sign_out
    find('.has-dropdown').click
    click_button I18n.t('navigation.sign_out')
  end

  def assert_user_signed_out
    assert_no_selector('[id="sign-out"]')
  end

  def assert_user_signed_in
    assert_selector('[id="sign-out"]', visible: :all)
  end

  def user_with_reset_password
    user = FactoryBot.create(:user)
    reset_password_for user.email
    user.reload
  end

  def reset_password_for(email)
    visit new_password_url
    fill_in 'password_email', with: email
    click_button I18n.t('helpers.submit.password.submit')
  end
end

class ActionDispatch::IntegrationTest
  include SignInHelper
end
