# frozen_string_literal: true

require 'application_system_test_case'

class ShowProfileTest < ApplicationSystemTestCase
  def setup
    @profile = create(:published_profile)
    @address = create(:address, profile_id: @profile.id, state: 'DE-HH')
    @tag = create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    @treatment_method = create(:treatment_method, name: {
                                 en: 'Abortion', de: 'Schwangerschaftsabbruch'
                               })
    create(:profile_treatment, treatment_method_id: @treatment_method.id,
                               profile_id: @profile.id)
    create(:tagging, tag_id: @tag.id, profile_id: @profile.id)
  end

  test 'displays the profile data' do
    visit profile_url(@profile.id)

    assert_text @profile.first_name
    assert_text @profile.last_name
    assert_text @profile.website
    assert_text @profile.comment
    assert_text @profile.language
    assert_text 'General Practicioner'
    assert_text @profile.phone_number
    assert_text @profile.financing_of_treatments
    assert_text @profile.accessibility
  end

  test 'displays the address' do
    visit profile_url(@profile.id)

    assert_text @address.street
    assert_text @address.zip
    assert_text @address.city
    assert_text 'Hamburg'
  end

  test 'displays the tags ' do
    visit profile_url(@profile.id)

    assert_text 'Trans friendly'
  end

  test 'displays the associated treatment methods ' do
    visit profile_url(@profile.id)

    assert_text 'Abortion'
  end

  test 'displays an edit button for a signed in user' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)

    visit profile_url(@profile.id)

    assert_link I18n.t('profile.edit')
  end

  test 'displays last updated date' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)

    visit profile_url(@profile.id)

    assert_text "Last updated: #{@profile.updated_at.strftime('%d %b %H:%M')}"
  end

  test 'does not display an edit button if user is not signed in' do
    visit profile_url(@profile.id)

    assert_no_link I18n.t('profile.edit')
  end
end
