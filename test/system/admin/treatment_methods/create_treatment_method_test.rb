# frozen_string_literal: true

require 'application_system_test_case'

class CreateTreatmentMethodTest < ApplicationSystemTestCase
  test 'redirects to treatment method index on success' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_treatment_method_path

    fill_in 'treatment_method_de', with: 'Behandlungsmethode'
    fill_in 'treatment_method_en', with: 'Example Treatment method'

    click_button I18n.t('admin.treatment_methods.form.save')

    assert_css 'td', text: 'Example Treatment method'
  end

  test 'requires de and en name' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_treatment_method_path

    click_button I18n.t('admin.treatment_methods.form.save')

    assert_text "Treatment method (english) can't be blank"
  end
end
