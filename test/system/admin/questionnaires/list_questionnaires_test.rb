# frozen_string_literal: true

require 'application_system_test_case'

class ListQuestionnairesTest < ApplicationSystemTestCase
  test 'lists all questionnaires' do
    answer1 = create(:answer, key: 'first_name', content: 'Hanni')
    answer2 = create(:answer, key: 'last_name', content: 'Example')
    answer3 = create(:answer, key: 'first_name', content: 'Hanni')
    answer4 = create(:answer, key: 'last_name', content: 'Example')
    create(:questionnaire, answers: [answer1, answer2])
    create(:questionnaire, answers: [answer3, answer4])
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    assert_selector(:css, 'table tbody tr', count: 2)
  end

  test 'displays the questionnaires status' do
    answer1 = create(:answer, key: 'first_name', content: 'Hanni')
    answer2 = create(:answer, key: 'last_name', content: 'Example')
    create(:questionnaire, status: :consultation_required,
                           answers: [answer1, answer2])
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    assert_text('Consultation required')
  end

  test 'links to edit/show questionnaire' do
    questionnaire = create(:questionnaire)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    assert_link nil, href: edit_admin_questionnaire_path(questionnaire)
  end

  test 'links to delete questionnaire' do
    questionnaire = create(:questionnaire)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    assert_link nil, href: admin_questionnaire_path(questionnaire)
  end

  test 'filtering by status' do
    create(:questionnaire, comment: 'I am a new questionnaire', status: :new)
    create(:questionnaire, comment: 'Consultation required',
                           status: :consultation_required)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    select I18n
      .t('admin.questionnaires.filter_bar.status.consultation_required'),
           from: 'status'
    click_button I18n.t('admin.questionnaires.filter_bar.filter')

    assert_no_text 'I am a new questionnaire'
    assert_text 'Consultation required'
  end

  test 'displays a pagination for more than 25 results' do
    create_list(:questionnaire, 26)
    user = create(:user)

    visit admin_questionnaires_path(as: user)

    assert_selector '.pagy-bulma-nav'
  end
end
