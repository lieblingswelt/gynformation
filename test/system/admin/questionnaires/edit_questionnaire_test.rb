# frozen_string_literal: true

require 'application_system_test_case'

class EditQuestionnairesTest < ApplicationSystemTestCase
  def setup
    @user = create(:user)
  end

  test 'allows admins to add status, comment and profile link' do
    questionnaire = create(:questionnaire)

    visit edit_admin_questionnaire_path(questionnaire.id, as: @user)

    fill_in 'questionnaire_comment', with: 'Done with the first review'
    select I18n.t('admin.questionnaires.edit.status.check_1_completed'),
           from: 'questionnaire_status'
    fill_in 'questionnaire_profile_link', with: 'https://www.gynformation.de/admin/profiles/1'

    click_button I18n.t('admin.questionnaires.edit.save')
  end

  test 'displays the treating persons basic data' do
    answer1 = create(:answer, key: 'first_name', content: 'Lora')
    answer2 = create(:answer, key: 'last_name', content: 'Meier')
    questionnaire = create(:questionnaire, answers: [answer1, answer2])

    visit edit_admin_questionnaire_path(questionnaire.id, as: @user)

    assert_text 'Lora Meier'
  end

  test 'formats health insurance answers' do
    answer = create(:answer, key: 'health_insurance',
                             content: ['private', '', 'statutory'])
    questionnaire = create(:questionnaire, answers: [answer])

    visit edit_admin_questionnaire_path(questionnaire.id, as: @user)

    assert_text 'private, statutory'
  end
end
