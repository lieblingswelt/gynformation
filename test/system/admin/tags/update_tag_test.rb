# frozen_string_literal: true

require 'application_system_test_case'

class UpdateTagTest < ApplicationSystemTestCase
  test 'redirects to tag index on success' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    treatment_method = create(:treatment_method)
    visit edit_admin_treatment_method_path(treatment_method.id)

    fill_in 'treatment_method_de', with: 'Neuer Behandlungsmethodenname'
    fill_in 'treatment_method_en', with: 'New treatment method name'

    click_button I18n.t('admin.treatment_methods.form.save')

    assert_css 'td', text: 'New treatment method name'
  end
end
