# frozen_string_literal: true

require 'application_system_test_case'

class ListTagsTest < ApplicationSystemTestCase
  test 'lists all tags' do
    _tag1 = create(:tag, key: 'trans_friendly',
                         name: {
                           en: 'Trans friendly',
                           de: 'Trans freundlich'
                         })
    _tag2 = create(:tag, key: 'sexworker_friendly',
                         name: {
                           en: 'Sex worker friendly',
                           de: 'Sexworkerinnen freundlich'
                         })
    user = create(:user)

    visit admin_tags_path(as: user)

    assert_selector(:css, 'table tbody tr', count: 2)
  end
end
