# frozen_string_literal: true

require 'application_system_test_case'

class CreateTagTest < ApplicationSystemTestCase
  test 'redirects to tag index on success' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_tag_path

    fill_in 'tag_de', with: 'Trans freundlich'
    fill_in 'tag_en', with: 'Trans friendly'

    click_button I18n.t('admin.tags.form.save')

    assert_css 'td', text: 'Trans friendly'
  end

  test 'requires de and en name' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_tag_path

    click_button I18n.t('admin.tags.form.save')

    assert_text "Tag name (english) can't be blank"
  end
end
