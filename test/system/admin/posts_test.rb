# frozen_string_literal: true

require 'application_system_test_case'

class AdminPostsTest < ApplicationSystemTestCase
  setup do
    @post = create(:post)
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
  end

  test 'visiting the index' do
    visit admin_posts_url
    assert_selector 'h1', text: 'Blog'
  end

  test 'creating a Post' do
    visit admin_posts_url
    click_on 'New Post'

    fill_in 'Author', with: @post.author
    fill_in 'Body', with: @post.body
    fill_in 'Teaser', with: @post.teaser
    fill_in 'Title', with: @post.title
    click_on 'Create Post'

    assert_text 'Post was successfully created'
    click_on 'Back'
  end

  test 'updating a Post' do
    visit admin_posts_url
    find('a[data-test="edit-post-link"]').click

    fill_in 'Author', with: @post.author
    fill_in 'Body', with: @post.body
    fill_in 'Teaser', with: @post.teaser
    fill_in 'Title', with: @post.title
    check 'post_published'
    click_on 'Update Post'

    assert_text 'Post was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Post' do
    visit admin_posts_url
    page.accept_confirm do
      find('a[data-test="delete-post-link"]').click
    end

    assert_text 'Post was successfully destroyed'
  end
end
