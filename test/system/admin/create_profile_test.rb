# frozen_string_literal: true

require 'application_system_test_case'

class CreateProfileTest < ApplicationSystemTestCase
  test 'redirects to created profile on success' do
    user = create(:user, email: 'admin@example.org')
    create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    create(:tag, name: { en: 'Fat friendly', de: 'Fat freundlich' })
    create(:treatment_method,
           name: { en: 'Abortion', de: 'Schwangerschaftsabbruch' })
    sign_in_with(user.email, user.password)
    visit new_admin_profile_path

    fill_form
    check 'profile_published'

    click_button I18n.t('profile.create')

    assert_text 'Lora'
    assert_text 'Ipsum'
    assert_text 'www.foobar.org'
    assert_text 'This is a comment'
    assert_text 'English'
    assert_text 'Example Street'
    assert_text '12345'
    assert_text 'Berlin'
    assert_text 'Trans friendly'
    assert page.has_no_button?(I18n.t('profile.create'))
  end

  test 'redirects to created profile in case of an unpublished profile' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)

    visit new_admin_profile_path
    fill_in 'profile_first_name', with: 'Lora'
    fill_in 'profile_last_name', with: 'Meier'
    select 'Berlin', from: 'profile_address_attributes_state'
    click_button I18n.t('profile.create')

    assert_text 'Lora Meier'
  end

  test 'displays an error message when name is missing' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    visit new_admin_profile_path

    fill_in 'profile_website', with: 'www.foobar.org'

    click_button I18n.t('profile.create')

    assert page.has_content?(
      I18n.t('activerecord.errors.models.profile.attributes.last_name.blank')
    )
  end

  def fill_form
    fill_in 'profile_first_name', with: 'Lora'
    fill_in 'profile_last_name', with: 'Ipsum'
    fill_in 'profile_address_attributes_street', with: 'Example Street'
    fill_in 'profile_address_attributes_zip', with: '12345'
    fill_in 'profile_address_attributes_city', with: 'Berlin'
    select 'Berlin', from: 'profile_address_attributes_state'
    fill_in 'profile_website', with: 'www.foobar.org'
    fill_in 'profile_phone_number', with: '040/2123214300923'
    fill_in 'profile_accessibility', with: 'Some information'
    fill_in 'profile_language', with: 'English'
    find('input#profile_health_insurance_uninsured',
         visible: false).set(true)
    fill_in 'profile_financing_of_treatments', with: 'Some content'
    fill_in 'profile_comment', with: 'This is a comment'
    select 'Trans friendly', from: 'profile_tag_ids'
    select 'Abortion', from: 'profile_treatment_method_ids'
  end
end
