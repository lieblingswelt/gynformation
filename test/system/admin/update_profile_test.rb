# frozen_string_literal: true

require 'application_system_test_case'

class UpdateProfileTest < ApplicationSystemTestCase
  def setup
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)
    create(:tag, name: { en: 'Trans friendly', de: 'Trans freundlich' })
    create(:tag, name: { en: 'Fat friendly', de: 'Fat freundlich' })
    @profile = create(:published_profile)
    @address = create(:address, profile_id: @profile.id)
  end

  test 'redirects to the created profile on success' do
    visit admin_profile_url(@profile.id)
    click_on I18n.t('profile.edit')

    fill_in 'profile_last_name', with: 'Dora'
    fill_in 'profile_first_name', with: 'The Explorer'
    fill_in 'profile_address_attributes_street', with: 'Update Street'
    fill_in 'profile_address_attributes_zip', with: '88888'
    fill_in 'profile_address_attributes_city', with: 'Hamburg'
    select 'Hamburg', from: 'profile_address_attributes_state'
    fill_in 'profile_website', with: 'www.updated-website.org'
    fill_in 'profile_phone_number', with: '1111111111111111111'
    fill_in 'profile_accessibility', with: 'Updated information about
     accessibility'
    fill_in 'profile_language', with: 'Another language'
    find('input#profile_health_insurance_uninsured',
         visible: false).set(true)
    fill_in 'profile_financing_of_treatments', with: 'Updated info about
    financing'
    fill_in 'profile_comment', with: 'This is an updated comment'
    select 'Fat friendly', from: 'profile_tag_ids'

    click_button I18n.t('profile.save')

    assert_text 'Dora'
    assert_text 'The Explorer'
    assert_text 'Update Street'
    assert_text '88888'
    assert_text 'Hamburg'
    assert_text 'www.updated-website.org'
    assert_text '1111111111111111111'
    assert_text 'Updated information about accessibility'
    assert_text 'Another language'
    assert_text I18n.t('questionnaire_form.uninsured')
    assert_text 'Updated info about financing'
    assert_text 'This is an updated comment'
    assert_text 'Fat friendly'
    assert_text 'Profile updated'
  end

  test 'update an unpublished profile' do
    unpublished_profile = create(:unpublished_profile)
    visit admin_profile_url(unpublished_profile)
    click_on I18n.t('profile.edit')

    fill_in 'profile_last_name', with: 'Updated Name'
    click_button I18n.t('profile.save')

    assert_text 'Updated Name'
    assert_text 'Profile updated'
  end

  test 'displays an error message when name is missing' do
    visit admin_profile_url(@profile.id)
    click_on I18n.t('profile.edit')

    fill_in 'profile_last_name', with: ''
    click_button I18n.t('profile.save')

    assert page.has_content?(
      I18n.t('activerecord.errors.models.profile.attributes.last_name.blank')
    )
  end
end
