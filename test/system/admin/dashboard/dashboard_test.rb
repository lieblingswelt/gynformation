# frozen_string_literal: true

require 'application_system_test_case'

class DashboardTest < ApplicationSystemTestCase
  test 'lists all profiles' do
    create(:published_profile, last_name: 'First Profile')
    create(:unpublished_profile, last_name: 'Second Profile')
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    assert_text 'First Profile'
    assert_text 'Second Profile'
  end

  test 'filtering profiles by published state' do
    create(:published_profile, last_name: 'I am a published profile')
    create(:unpublished_profile, last_name: 'Hey, I am unpublished')
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    select I18n.t('profile.published.false'), from: 'published'
    click_button I18n.t('profile.filter')

    assert_no_text 'I am a published profile'
    assert_text 'Hey, I am unpublished'
  end

  test 'searching for profiles by last name' do
    create(:profile, last_name: 'Schmidt')
    create(:profile, last_name: 'Meier')
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    fill_in 'last_name', with: 'Meier'
    click_button I18n.t('profile.filter')

    assert_no_text 'Schmidt'
    assert_text 'Meier'
  end

  test 'profiles list entry links to edit profile' do
    profile = create(:profile)
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    assert_link nil, href: edit_admin_profile_path(profile)
  end

  test 'links to create profile' do
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    assert_link I18n.t('admin.new_profile'), href: new_admin_profile_path
  end

  test 'links to delete profile' do
    profile = create(:profile)
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    assert_link nil, href: admin_profile_path(profile)
  end

  test 'links to show profile' do
    profile = create(:profile)
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    assert_link nil, href: admin_profile_path(profile)
  end

  test 'displays a pagination for more than 25 results' do
    create_list(:profile, 26)
    user = create(:user)

    visit admin_dashboard_index_path(as: user)

    assert_selector '.pagy-bulma-nav'
  end
end
