# frozen_string_literal: true

require 'application_system_test_case'

class NavigationsTest < ApplicationSystemTestCase
  test 'top navigation' do
    visit root_url

    assert_link I18n.t('navigation.search'), href: profiles_path
    assert_link I18n.t('navigation.submit'), href: new_questionnaire_path
    assert_link I18n.t('navigation.blog'), href: posts_path
    assert_link I18n.t('navigation.about'), href: about_path
    assert_link I18n.t('navigation.faq'), href: faq_path
    assert_link I18n.t('navigation.glossary'), href: glossary_path
  end
end
