# frozen_string_literal: true

require 'application_system_test_case'

class ShowPostTest < ApplicationSystemTestCase
  def setup
    @post = create(:published_post)
  end

  test 'displays title, author, teaser and body' do
    visit post_url(@post.id)

    assert_text @post.title
    assert_text @post.author
    assert_text @post.body
  end
end
