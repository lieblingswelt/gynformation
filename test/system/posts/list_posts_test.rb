# frozen_string_literal: true

require 'application_system_test_case'

class ListProfilesTest < ApplicationSystemTestCase
  def setup
    @post = create(:published_post)
  end

  test 'visiting the index' do
    visit posts_url

    assert_selector 'ul', class: 'post__list'
  end

  test 'displays title, author and teaser ' do
    visit posts_url

    assert_text @post.title
    assert_text @post.teaser
  end
end
