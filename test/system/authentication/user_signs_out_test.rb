# frozen_string_literal: true

require 'application_system_test_case'
require 'test_helper'

class UserSignsOutTest < ApplicationSystemTestCase
  test 'User signs out' do
    user = create(:user, email: 'admin@example.org')
    sign_in_with(user.email, user.password)

    sign_out

    assert_user_signed_out
  end
end
