# frozen_string_literal: true

require 'application_system_test_case'
require 'test_helper'

class UserSignsInTest < ApplicationSystemTestCase
  test 'User signs in with valid email and password' do
    user = create(:user, email: 'admin@example.org')

    sign_in_with(user.email, user.password)

    assert_user_signed_in
  end

  test 'User signs in with invalid password' do
    user = create(:user, email: 'user@example.com', password: 'password')

    sign_in_with(user.email, 'wrong password')

    assert_user_signed_out
    assert_text I18n.t('flashes.failure_after_create')
  end

  test 'User signs in with invalid email' do
    user = create(:user, email: 'user@example.com', password: 'password')

    sign_in_with('wrong@example.org', user.password)

    assert_user_signed_out
    assert_page_displays_sign_in_error
  end

  private

  def assert_page_displays_sign_in_error
    assert_text I18n.t('flashes.failure_after_create')
  end
end
