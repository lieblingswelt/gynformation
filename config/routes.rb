# frozen_string_literal: true

Rails.application.routes.draw do
  resources :passwords, controller: 'clearance/passwords', only: %i[create new]
  resource :session, controller: 'clearance/sessions', only: [:create]

  resources :users, controller: 'clearance/users', only: [:create] do
    resource :password,
             controller: 'clearance/passwords',
             only: %i[create edit update]
  end

  get '/sign_in' => 'clearance/sessions#new', as: 'sign_in'
  delete '/sign_out' => 'clearance/sessions#destroy', as: 'sign_out'

  root 'static_pages#home'
  get 'static_pages/home', to: 'static_pages#home'
  get '/about', to: 'static_pages#about'
  get '/faq', to: 'static_pages#faq'
  get '/imprint', to: 'static_pages#imprint'
  get '/data-protection', to: 'static_pages#data_protection'
  get '/contact', to: 'static_pages#contact'
  get '/glossary', to: 'static_pages#glossary'

  resources :profiles, only: %i[index show]
  resources :questionnaire, only: %i[new create]
  get 'questionnaire/info', to: 'questionnaire#info'
  resources :posts, only: %i[index show]

  constraints Clearance::Constraints::SignedIn.new do
    namespace :admin do
      resources :profiles
      resources :dashboard, only: %i[index]
      resources :questionnaires, only: %i[index edit update destroy]
      resources :tags
      resources :treatment_methods
      resources :posts
    end
  end
end
