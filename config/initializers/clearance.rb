# frozen_string_literal: true

Clearance.configure do |config|
  config.routes = false
  config.mailer_sender = 'kontakt@gynformation.de'
  config.rotate_csrf_on_sign_in = true
  config.allow_sign_up = false
end
