# frozen_string_literal: true

namespace :questionnaire do
  desc 'Change the answer structure for existing answers to hash with boolean
    and comment key'
  task add_comment_to_existing_boolean_answers: :environment do
    def log(msg)
      puts msg
      Rails.logger.info msg
    end

    log('Starting task to change the answer structure for existing answers to hash with boolean
    and comment key')

    start_time = Time.now 
    affected_answers = Answer.all.where(key: ['listening', 'consent', 'financing_of_treatments'])

    affected_answers.each do |answer| 
      unless answer.content.is_a?(Hash)
        old_content = answer.content
        answer.update!(content: { boolean: old_content, comment: ''})
      end
    end

    log("Task completed in #{Time.now - start_time} seconds.")
  end
end
