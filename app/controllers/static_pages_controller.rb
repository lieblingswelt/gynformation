# frozen_string_literal: true

class StaticPagesController < ApplicationController
  def home; end

  def about; end

  def faq; end

  def imprint; end

  def data_protection; end

  def contact; end

  def glossary; end
end
