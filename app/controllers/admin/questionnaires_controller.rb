# frozen_string_literal: true

class Admin::QuestionnairesController < ApplicationController
  layout 'admin'

  def index
    @pagy, @questionnaires = pagy(
      Questionnaire.filter(params.slice(:status))
    )
  end

  def edit
    @questionnaire = Questionnaire.find(params[:id])
  end

  def update
    @questionnaire = Questionnaire.find(params[:id])
    if @questionnaire.update_attributes(questionnaire_params)
      flash[:success] = 'Questionnaire updated'
      redirect_to edit_admin_questionnaire_path(@questionnaire)
    else
      render 'edit'
    end
  end

  def destroy
    @questionnaire = Questionnaire.find(params[:id])
    @questionnaire.destroy
    redirect_to admin_questionnaires_path
  end

  private

  def questionnaire_params
    params.require(:questionnaire).permit(:status, :comment, :profile_link)
  end
end
