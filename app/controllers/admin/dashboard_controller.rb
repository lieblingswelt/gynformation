# frozen_string_literal: true

class Admin::DashboardController < ApplicationController
  layout 'admin'
  def index
    @pagy, @profiles = pagy(
      Profile.filter(params.slice(:published, :last_name))
    )
  end
end
