# frozen_string_literal: true

class Admin::ProfilesController < ApplicationController
  layout 'admin'

  def new
    @profile = Profile.new
    @profile.build_address
  end

  def show
    @profile = Profile.find(params[:id])
  end

  def edit
    @profile = Profile.find(params[:id])
  end

  def create
    @profile = Profile.new(profile_params)

    if @profile.save
      redirect_to admin_profile_path(@profile)
    else
      render 'new'
    end
  end

  def update
    @profile = Profile.find(params[:id])
    if @profile.update_attributes(profile_params)
      flash[:success] = 'Profile updated'
      redirect_to admin_profile_path(@profile)
    else
      render 'edit'
    end
  end

  def destroy
    @profile = Profile.find(params[:id])
    @profile.destroy
    redirect_to admin_dashboard_index_path
  end

  private

  def profile_params
    params.require(:profile).permit(
      :first_name, :last_name, :website, :category, :published, :comment,
      :language, :tag_list, :tag, { tag_ids: [] }, :tag_ids, :phone_number,
      :accessibility, :financing_of_treatments,
      :treatment_method_id, { treatment_method_ids: [] }, :treatment_method_ids,
      address_attributes: %i[id profile_id street zip city state],
      health_insurance: []
    )
  end
end
