# frozen_string_literal: true

class ProfilesController < ApplicationController
  def index
    @pagy, @profiles = pagy(Profile.filter(
      params.slice(:tag, :state, :treatment_method)
    ).where(published: true).order(:last_name))
  end

  def show
    @profile = Profile.where(published: true).find(params[:id])
  end

  private

  def profile_params
    params.require(:profile).permit(:tag, :state, :treatment_method)
  end
end
