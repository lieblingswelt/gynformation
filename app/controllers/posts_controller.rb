# frozen_string_literal: true

class PostsController < ApplicationController
  def show
    @post = Post.where(published: true).find(params[:id])
  end

  def index
    @posts = Post.where(published: true)
  end
end
