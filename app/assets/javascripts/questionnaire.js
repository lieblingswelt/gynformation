// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

document.addEventListener('turbolinks:load', function (event) {
  let steps = new bulmaSteps('#questionnaire-steps', {
    onShow: function(step_id) {
      scroll(0,0)
    }
  })
})