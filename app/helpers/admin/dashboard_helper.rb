# frozen_string_literal: true

module Admin::DashboardHelper
  def options_for_published
    {
      t('profile.published.all') => nil,
      t('profile.published.true') => true,
      t('profile.published.false') => false
    }
  end
end
