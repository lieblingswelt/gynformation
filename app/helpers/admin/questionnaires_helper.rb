# frozen_string_literal: true

module Admin::QuestionnairesHelper
  def formatted_tags(tags)
    return if tags.blank?

    tags.reject(&:blank?).map do |tag_name|
      content_tag(:span, t("questionnaire_form.tags.#{tag_name}"), class: 'tag')
    end.join
  end

  def format_recommended_treatments(treatments)
    return if treatments.blank?

    treatments.reject(&:blank?).map do |tag_name|
      content_tag(:span, t("questionnaire_form.treatments.#{tag_name}"))
    end.join(', ')
  end

  def format_examination_execution(content)
    return if content.blank?

    content.reject(&:blank?).map do |answer|
      content_tag(:li, t("questionnaire_form.examination_execution.#{answer}"))
    end.join
  end

  def format_health_insurance(content)
    return if content.blank?

    content.reject(&:blank?).map do |answer|
      content_tag(
        :span,
        t("admin.questionnaires.edit.health_insurance_options.#{answer}")
      )
    end.join(', ')
  end

  def format_boolean_and_comment_answer(answer)
    return if answer.blank?

    answer.to_a.map do |key, value|
      content_tag(:li, "#{t(".answer_type.#{key}")}: #{value}")
    end.join
  end

  def options_for_status
    {
      t('.status.all') => nil,
      t('.status.new') => :new,
      t('.status.in_progress') => :in_progress,
      t('.status.check_1_completed') => :check_1_completed,
      t('.status.consultation_required') => :consultation_required,
      t('.status.profile_published') => :profile_published,
      t('.status.inquired_about_abortion') => :inquired_about_abortion
    }
  end
end
