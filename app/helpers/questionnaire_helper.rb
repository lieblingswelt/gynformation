# frozen_string_literal: true

module QuestionnaireHelper
  def insurance_options
    {
      statutory: t('questionnaire_form.statutory'),
      private: t('questionnaire_form.private'),
      uninsured: t('questionnaire_form.uninsured'),
      unknown: t('questionnaire_form.unknown')
    }
  end

  def tag_list
    {
      fgmc: t('questionnaire_form.tags.fgmc'),
      non_binary: t('questionnaire_form.tags.non_binary'),
      trans: t('questionnaire_form.tags.trans'),
      inter: t('questionnaire_form.tags.inter'),
      black_people: t('questionnaire_form.tags.black_people'),
      poc: t('questionnaire_form.tags.poc'),
      jewish: t('questionnaire_form.tags.jewish'),
      mentally_retarded: t('questionnaire_form.tags.mentally_retarded'),
      migrant: t('questionnaire_form.tags.migrant'),
      muslim: t('questionnaire_form.tags.muslim'),
      physically_disabled: t('questionnaire_form.tags.physically_disabled'),
      roma: t('questionnaire_form.tags.roma'),
      gay_bi_or_pan: t('questionnaire_form.tags.gay_bi_or_pan'),
      asexual: t('questionnaire_form.tags.asexual'),
      polyamorous: t('questionnaire_form.tags.polyamorous'),
      sex_worker: t('questionnaire_form.tags.sex_worker'),
      single_parent: t('questionnaire_form.tags.single_parent'),
      drug_user: t('questionnaire_form.tags.drug_user'),
      fat: t('questionnaire_form.tags.fat'),
      thin: t('questionnaire_form.tags.thin'),
      hiv_positive: t('questionnaire_form.tags.hiv_positive'),
      chronically_ill: t('questionnaire_form.tags.chronically_ill'),
      mental_illness: t('questionnaire_form.tags.mental_illness'),
      sexual_violence: t('questionnaire_form.tags.sexual_violence')
    }
  end

  def treatments_list
    {
      abortion_dileation: t('questionnaire_form.treatments.abortion_dileation'),
      abortion_aspiration:
      t('questionnaire_form.treatments.abortion_aspiration'),
      abortion_medication:
      t('questionnaire_form.treatments.abortion_medication'),
      breastfeeding: t('questionnaire_form.treatments.breastfeeding'),
      cancer: t('questionnaire_form.treatments.cancer'),
      contraception_methods:
      t('questionnaire_form.treatments.contraception_methods'),
      endometriosis: t('questionnaire_form.treatments.endometriosis'),
      initial_treatment: t('questionnaire_form.treatments.initial_treatment'),
      menopause: t('questionnaire_form.treatments.menopause'),
      mental_counseling: t('questionnaire_form.treatments.mental_counseling'),
      miscarriage: t('questionnaire_form.treatments.miscarriage'),
      pregnancy: t('questionnaire_form.treatments.pregnancy'),
      routine_treatment: t('questionnaire_form.treatments.routine_treatment'),
      sexual_dysfunction: t('questionnaire_form.treatments.sexual_dysfunction'),
      spiral_insertion: t('questionnaire_form.treatments.spiral_insertion'),
      sterilization: t('questionnaire_form.treatments.sterilization'),
      std: t('questionnaire_form.treatments.std'),
      std_test: t('questionnaire_form.treatments.std_test'),
      transition: t('questionnaire_form.treatments.transition'),
      unfulfilled_desire_children_hetero:
      t('questionnaire_form.treatments.unfulfilled_desire_children_hetero'),
      unfulfilled_desire_children_queer:
      t('questionnaire_form.treatments.unfulfilled_desire_children_queer')

    }
  end

  def boolean_options
    {
      yes: t('questionnaire_form.answer_yes'),
      no: t('questionnaire_form.answer_no')
    }
  end

  def financing_of_treatment_options
    {
      yes: t('questionnaire_form.answer_yes'),
      no: t('questionnaire_form.answer_no'),
      not_applicable: t('questionnaire_form.answer_not_applicable')
    }
  end

  def examination_execution_options
    {
      examine_yourself:
      t('questionnaire_form.examination_execution.examine_yourself'),
      lateral_position:
      t('questionnaire_form.examination_execution.lateral_position'),
      stay_clothed: t('questionnaire_form.examination_execution.stay_clothed')
    }
  end

  def category_options
    {
      t('questionnaire_form.category.general_practitioner') =>
      :general_practitioner,
      t('questionnaire_form.category.gynecologist') => :gynecologist,
      t('questionnaire_form.category.midwife') => :midwife
    }
  end
end
