# frozen_string_literal: true

class NullAnswer
  def initialize(key)
    @key = key
  end

  def content
    ''
  end
end
