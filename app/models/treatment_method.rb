# frozen_string_literal: true

class TreatmentMethod < ApplicationRecord
  has_many :profile_treatments
  has_many :profiles, through: :profile_treatments

  store_accessor :name, :de, :en

  validates :en, presence: true
  validates :de, presence: true
end
