# frozen_string_literal: true

class Profile < ApplicationRecord
  include Filterable
  has_one :address, dependent: :destroy
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings
  has_many :profile_treatments, dependent: :destroy
  has_many :treatment_methods, through: :profile_treatments
  attribute :published, :boolean, default: false

  accepts_nested_attributes_for :address

  delegate :street, :city, :state, :zip, to: :address, allow_nil: true

  enum category: %i[gynecologist general_practitioner midwife]

  validates :last_name, presence: true

  scope :filter_by_tag, ->(key) { joins(:tags).where('tags.key' => key) }
  scope :filter_by_state, lambda { |state|
                            includes(:address)
                              .where('addresses.state' => state)
                          }
  scope :filter_by_treatment_method, lambda { |key|
                                       joins(:treatment_methods)
                                         .where('treatment_methods.key' => key)
                                     }
  scope :filter_by_published, ->(published) { where published: published }
  scope :filter_by_last_name,
        ->(last_name) { where('last_name like ?', "#{last_name}%") }

  def tag_list
    tags.map(&:name).join(', ')
  end
end
