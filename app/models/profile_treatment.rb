# frozen_string_literal: true

class ProfileTreatment < ApplicationRecord
  belongs_to :treatment_method
  belongs_to :profile
end
