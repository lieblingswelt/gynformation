# frozen_string_literal: true

class Questionnaire < ApplicationRecord
  include Filterable

  attribute :status, :integer, default: 1
  has_many :answers, dependent: :destroy
  enum status: { new: 1, check_1_completed: 2, consultation_required: 4,
                 profile_published: 5, in_progress: 6,
                 inquired_about_abortion: 7 }, _prefix: :status

  default_scope { order(created_at: :desc) }
  scope :filter_by_status, ->(status) { where status: status }

  def treating_persons_name
    return unless names_present?

    answers.where(key: 'first_name').first.content << ' ' <<
      answers.where(key: 'last_name').first.content
  end

  def answer(key)
    answer = answers.where(key: key).first || NullAnswer.new(key)
    answer.content
  end

  private

  def names_present?
    answers.where(key: 'first_name').first &&
      answers.where(key: 'last_name').first
  end
end
