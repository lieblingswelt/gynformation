# frozen_string_literal: true

class QuestionnaireForm
  include ActiveModel::Model
  ATTRIBUTES = %i[first_name last_name category street zip_code
                  city state website payments_required_for_specific_treatments
                  languages barrier_free_access health_insurance
                  self_describing_tags recommended_treatments treatment_rejected
                  financing_of_treatments listening consent
                  examination_execution treated_with_respect
                  discriminatory_remarks respect_anything_else feedback
                  additional_self_describing_tags explanation_of_treatment
                  additional_recommended_treatments practice_staff
                  limit_recommendation phone_number].freeze

  attr_accessor *ATTRIBUTES

  def save
    return false if invalid?

    create_questionnaire
    true
  end

  private

  def create_questionnaire
    ActiveRecord::Base.transaction do
      questionnaire = Questionnaire.new
      ATTRIBUTES.each do |attribute|
        questionnaire.answers.build(
          key: attribute.to_s, content: method(attribute).call
        )
      end
      questionnaire.save!
    end
  end
end
