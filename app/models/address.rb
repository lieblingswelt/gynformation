# frozen_string_literal: true

class Address < ApplicationRecord
  def self.states
    STATES
  end

  STATES = [
    { iso_code: 'DE-BW',
      name: 'Baden-Württemberg' },
    { iso_code: 'DE-BY',
      name: 'Bayern' },
    { iso_code: 'DE-BE',
      name: 'Berlin' },
    { iso_code: 'DE-BB',
      name: 'Brandenburg' },
    { iso_code: 'DE-HB',
      name: 'Bremen' },
    { iso_code: 'DE-HH',
      name: 'Hamburg' },
    { iso_code: 'DE-HE',
      name: 'Hessen' },
    { iso_code: 'DE-MV',
      name: 'Mecklenburg-Vorpommern' },
    { iso_code: 'DE-NI',
      name: 'Niedersachsen' },
    { iso_code: 'DE-NW',
      name: 'Nordrhein-Westfalen' },
    { iso_code: 'DE-RP',
      name: 'Rheinland-Pfalz' },
    { iso_code: 'DE-SL',
      name: 'Saarland' },
    { iso_code: 'DE-SN',
      name: 'Sachsen' },
    { iso_code: 'DE-ST',
      name: 'Sachsen-Anhalt' },
    { iso_code: 'DE-SH',
      name: 'Schleswig-Holstein' },
    { iso_code: 'DE-TH',
      name: 'Thüringen' }
  ].freeze

  validates :state, inclusion: { in: STATES.map { |state| state[:iso_code] },
                                 message: '%<value>s is not a valid state' }
end
