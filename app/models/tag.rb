# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :taggings
  has_many :profiles, through: :taggings

  store_accessor :name, :de, :en

  validates :en, presence: true
  validates :de, presence: true
end
