# README

## About Gynformation

Gynformation is a a curated web directory to find gynecological treatment. We are a group of queer-feminist activists who advocate self-determination in the context of gynaecological treatment from a patient perspective.  

## Development setup

* Install [ruby](https://www.ruby-lang.org/en/documentation/installation/)  
* Install [postgres](https://www.postgresql.org/) and make sure it is running locally.
* Install [bundler](https://bundler.io)
* Clone this repository
* Run `bundle install`  inside of the project directory to install all dependencies and `rake db:setup` to initialize the database.
* Run `rake db:seed` to populate the database
* For Windows: "workers Integer(ENV['WEB_CONCURRENCY'] || 2)" might have to be removed from puma.rb
* Start the development server `bin/rails server` and visit

## Testing

Run `bin/rails test` to run the tests, use `bin/rails test:system test` to include the systems tests using headless chrome.

## Linting

We use [rubocop](https://github.com/rubocop-hq/rubocop) to lint the code. 

`gem install rubocop`

The checks run in the pipeline, to check locally run:

`rubocop -a`

## Deployment

The master branch ist continously deployed to our staging environment.
Production deployment needs manual approval in the gitlab pipeline. 
